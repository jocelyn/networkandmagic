# Network & Magic

Network & Magic est un jeu de rôle qui propose aux participants de prendre chacun le rôle d'un ordinateur sur Internet. L'objectif du groupe sera de réussir à afficher une page Web en communiquant comme le font les machines sur le réseau.

## Objectifs : Compréhension & Sensibilisation

* Comprendre les bases du fonctionnement d'Internet
* Se représenter physiquement les équipements qui constituent Internet
* Sensibiliser sur la problématique de la collecte de données à l'insue des usagers

## Publics visés

Adultes et ados à partir de 13 ans

## Nombre de participants

5 à 18 personnes (12 est un nombre idéal)

## Pré-requis

Utiliser ou avoir utilisé Internet "régulièrement" (au moins 30 min par mois)

## Durée

40 minutes (+ 30 minutes de questions/réponses sauf si une conférence suit)

## Préparation sur place

20 minutes (si on a l'habitude)

## Lieu et matériel nécessaire

* un espace au sol de 5 m sur 5 m
* un lieu où l'animateur pourra se faire entendre de tous (pas trop bruyant)
* Si possible : des chaises pour les participants et une seule petite table
* Ni Internet, ni vidéo projecteur ne sont nécessaires

## Todo

* remplacer certaines images couvertes par un copyright...
